.. _part3:

*************************************************************************************************
Partie 3 | Semantic Analysis
*************************************************************************************************

Questions proposed by Group 27, Jacques Yakoub & Alexandre Dewit
================================================================

1. Using a code sample, explain how can we handle context with context objects
------------------------------------------------------------------------------

2. Explain what is variable shadowing and how can we detect it 
--------------------------------------------------------------


Answers
"""""""

1. Using this example : 

.. literalinclude:: Factorial.java
    :linenos:
    :language: java

* CompilationUnitContext on lines 1-16.
* ClassContext on lines 3-16.
* MethodContext on lines 5-8 and 10-13.

At any point of execution, we have a stack of Context's.
That is useful for example if we lookup for the definition of a variable (like the 'n' in the code sample).

+------------------------+
| CompilationUnitContext |
+------------------------+
| ClassContext           |
+------------------------+
| MethodContext          |
+------------------------+
| LocalContext1          |
+------------------------+
| LocalContext2          |
+------------------------+


2. 

Definition from `Wikipedia <https://en.wikipedia.org/wiki/Variable_shadowing>`_ : 
Variable shadowing occurs when a variable declared within a certain scope has the same name as a variable declared in an outer scope

For the detection part, check the course slides at page 36.